/// <reference types="cypress" />

import {resultsPageElements} from '../pageLocators/resultsPage'
import {searchPageElements} from '../pageLocators/searchPage'

function search(text)  {
    cy.get(searchPageElements.searchField)
     .should('be.visible')
     .clear()
     .type(text + "{enter}")
}

context("Web Search", function() {
    
    beforeEach( function () {
        cy.visit("/")
        cy.title().should('eq', 'Google')
        cy.fixture('search.json').then( $data => {
            cy.wrap($data.all.textSearch1).as('textTest1')
            cy.wrap($data.all.textSearch2).as('textTest2')
            cy.wrap($data.all.textSearch3).as('textTest3')
        })
    })

    describe("Search and validate result title", function () {
    
        it("Search text 1", function () {
            search(this.textTest1)
            cy.get(resultsPageElements.resultElementsTitle)
              .should('be.visible')
              .first()
              .should('contain.text', this.textTest1)
        })
    
        it("Search text 2", function () {
            search(this.textTest2)
            cy.get(resultsPageElements.resultElementsTitle)
              .should('be.visible')
              .first()
              .should('contain.text', this.textTest2)
        })
    
        it("Search text 3", function () {
            search(this.textTest3)
            cy.get(resultsPageElements.resultElementsTitle)
              .should('be.visible')
              .first()
              .should('contain.text', this.textTest3)
        })
    })
})
