/// <reference types="cypress" />

function searchABook(bookName)  {
    const firstBookResult = 'tr > :nth-child(1)'
    cy.get(firstBookResult).clear
    cy.get(firstBookResult).type(bookName + "{enter}")
}

function clickOnBookByName(bookName) {
    debugger
    cy.get('#search h3').contains(bookName).should('be.visible').click()
}

describe("Book Search", function () {

    beforeEach( function () {
        cy.visit("https://books.google.com")
        cy.title().should('eq', 'Google Books')
        cy.fixture('search.json').then( $data => {
            cy.wrap($data.books.bookName1).as('bookName1')
            cy.wrap($data.books.bookAuthor1).as('bookAuthor1')
        })
    })

    it("Search for book title", function () {
        searchABook(this.bookName1)
        cy.get('#search h3').first().should('contain.text', this.bookName1)
    })

    it("Search for book author", function () {
        searchABook(this.bookName1)
        cy.get('#search a[href*="author"]').should('contain.text', this.bookAuthor1)
    })
})
